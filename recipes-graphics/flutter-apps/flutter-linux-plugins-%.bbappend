# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

do_compile[network] = "1"
do_unpack[network] = "1"
do_install[network] = "1"
